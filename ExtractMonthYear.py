import re
import os
import sys

DAY_PATTERN = r"\b(0{0,1}[1-9]|1\d|2\d|3{0,1})"
BREAK_PATTERN = r"\s{0,}(-|\/|,){0,}\s{0,}"
YEARN_PATTERN = r"\b(20(0|1)\d|19(8|9)\d|9\d|0\d|1\d)\b"
YEARN_PATTERN = r"\b(20(0|1)\d|19(8|9)\d|9\d)\b"

MONTH_PATTERN = r"\b(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|jun(e)?|jul(y)?|aug(ust)?|sep(tember)?|oct(ober)?|nov(ember)?|dec(ember)?)\b"
MONTH_NUMBER_PATTERN = r"\b(0\d|10|11|12|\d)\b"

CURRENT_TIME = "(hiện nay|hiện tại|now|present|current)"


pattern0_1 = re.compile(MONTH_NUMBER_PATTERN+BREAK_PATTERN+DAY_PATTERN+BREAK_PATTERN+YEARN_PATTERN)
pattern0_2 = re.compile(DAY_PATTERN+BREAK_PATTERN+MONTH_NUMBER_PATTERN+BREAK_PATTERN+YEARN_PATTERN)
pattern0_3 = re.compile(YEARN_PATTERN+BREAK_PATTERN+MONTH_NUMBER_PATTERN+BREAK_PATTERN+DAY_PATTERN)
ignore_patterns = [pattern0_1, pattern0_2, pattern0_3]
pattern1 = re.compile(MONTH_PATTERN+BREAK_PATTERN+YEARN_PATTERN)
pattern2 = re.compile(YEARN_PATTERN+BREAK_PATTERN+MONTH_PATTERN)
pattern3 = re.compile(MONTH_NUMBER_PATTERN+BREAK_PATTERN+YEARN_PATTERN)
pattern4 = re.compile(YEARN_PATTERN+BREAK_PATTERN+MONTH_NUMBER_PATTERN)
pattern5 = re.compile(YEARN_PATTERN)
pattern6 = CURRENT_TIME
all_pattern = [pattern1, pattern2, pattern3, pattern4, pattern5, pattern6]

def get_month_year(text):
    text = text.lower()
    # for ignore_pattern in ignore_patterns:
    #     if re.search(ignore_pattern,text):
    #         print('right here')
    #         return []
    for ind, pattern in enumerate(all_pattern):
        result = re.search(pattern, text)
        if result:
            month_years = [x.group() for x in re.finditer(pattern, text)]
            if len(month_years) == 1 and ind != 5:
                current_time_result = re.search(pattern6, text)
                if current_time_result:
                    month_years += [current_time_result.group(0)]

            return month_years

    return []


def main():
    text1 = "hallo thang 8 2001"
    text2 = "haha march, 2018"
    text3 = "haha 2018-march"
    text4 = "haha 09/2018"
    text5 = " lala 2018-10 lala 2018-12"
    text6 = "05/2018 - 6/2019"
    text7 = "October 2018\xa0-\xa0present\xa0"
    text8 = "January 2017\xa0-\xa0December 2017\xa0(1 year)' 'Hanoi"
    text9 = "haha 13/02/1995"
    for ind, text in enumerate([text1, text2, text3, text4, text5, text6, text7, text8, text9]):
        print(ind)
        print(get_month_year(text))



if __name__ == '__main__':
    main()
