import pandas as pd
import numpy as np
import os
from utils import get_line_by_line_text, remove_accents, company_keyword_processor, get_special_line,\
                  get_case_sensitive_keyword
from ExtractMonthYear import get_month_year
import subprocess

import warnings
warnings.filterwarnings('ignore')



def main():
    # original_path = '/Users/quan/Desktop/jobhop/education/new_location/test_data'
    # original_path = '/Users/quan/Desktop/jobhop_2/2020/10/ai_parser/sample_data'
    original_path = '/Users/quan/Desktop/full_parsing_strategy/cv_parser/List of CV'
    all_files = os.listdir(original_path)
    all_files = [os.path.join(original_path, f) for f in all_files if f.endswith('.pdf')]
    individual_file = np.random.choice(all_files)
    individual_file = os.path.join(original_path, 'buihungmanh_giam doc du an xay dung.pdf')
    # individual_file = '/Users/quan/Desktop/jobhop_2/2020/10/ai_parser/sample_data/7770752.pdf'
    # individual_file = '/Users/quan/Desktop/jobhop_2/2020/10/ai_parser/sample_data/3801098.pdf'
    print(individual_file)
    subprocess.call(['open', individual_file]) 
    df = get_line_by_line_text(individual_file)
    print(df['text'].values.tolist())
    df['month_year'] = df['text'].apply(get_month_year)
    mask_month_year = df['month_year'].apply(lambda x: len(x) !=0)
    print(df[mask_month_year])
if __name__ == "__main__":
    main()