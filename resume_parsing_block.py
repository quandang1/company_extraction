import fitz                 # this is PyMuPDF
import sys, json
import pandas as pd
from sklearn.cluster import DBSCAN
import warnings
import numpy as np
from copy import deepcopy
import re
import warnings
warnings.filterwarnings('ignore')
# from colorama import Fore, Style
import pprint


def filter_sensitive(text, cond):
    """ Filter case sensitive text based on cond
    
    Input:
    text: input text
    cond: condition, upper or capitalize
    
    Return:
    True or False
    """
    if cond == 'upper':
        count = 0
        for char in text:
            if char.upper() == char:
                count +=1
        if count/len(text) > 0.8:
            return True
        return False
    if cond == 'capitalize':
        if text.split(' ')[0] == text.split(' ')[0].capitalize():
            return True
        count = 0
        for char in text:
            if char.upper() == char:
                count +=1
        if count/len(text) > 0.8:
            return True
        return False
# list_heading = np.load('SAP_info/data/list_heading.npy')
# list_heading = list(list_heading)
# list_heading +=['k i n h n g h i ệ m', 'introduction', 'kinh nghiệm làm việc']
# list_heading += ['Mục tiêu nghề nghiệp', 'Objectives', 'Career objective', 'Career objectives', 'Objective', 'MỤC TIÊU']
list_heading = []
def filter_heading(heading_df, list_heading=list_heading):
    """ Removed redundant rows non-including predefined heading keywords of given heading dataframe
    Input:
    heading_df: heading dataframe
    list_heading: list of pre-defined heading keywords
    Ouput:
    Heading dataframe, which removed redundant rows non-including predefined heading keywords
    """
    # mask_length = heading_df['text'].apply(lambda x: len(x.split(' '))) <= 5
    # heading_df = heading_df[mask_length]

    mask_heading = heading_df['text'].apply(lambda x: np.any([remove_accents_non_space(f) in remove_accents_non_space(x) for f in list_heading]))

    return heading_df[mask_heading]

#create variation of heading keywords

with open('data/list_heading.json') as json_file:
    json_data = json.loads(json_file.read())
json_data_new = {}
for block_name in json_data.keys():
    heading_key_words = json_data[block_name] + [block_name]
    heading_key_words = [f for f in heading_key_words if f != '']
    heading_key_words.sort(key = lambda s: len(s.split( )), reverse=True)
    heading_new = []
    for heading_word in heading_key_words:
        temp_word = heading_word.upper()
        heading_new.append(temp_word)
    for heading_word in heading_key_words:
        temp_word = heading_word.title()
        heading_new.append(temp_word)
    for heading_word in heading_key_words:
        temp_word = heading_word.capitalize()
        heading_new.append(temp_word)
    json_data_new[block_name] = heading_new


def filter_strict(text, case_sensitive):
    # extract heading keyword from candiate rows
    heading_keyword = ''
    heading_block_keyword = ''
    for block_heading in ['working history', 'education', 'skills', 'information', 'objective',\
                         'reference', 'awards', 'hobby', 'volunteer', 'project']:
        for block_heading_variation in json_data_new[block_heading]:
            if block_heading_variation == '':
                continue
            if block_heading_variation in text:
                heading_keyword = block_heading_variation
                heading_block_keyword = block_heading
                break
        if heading_keyword != '':
            break
    if heading_keyword == '':
        return False
    text = re.sub(r'[^\w]', ' ', text)
    text = re.sub(r'\s+', ' ', text)
    text = text.strip()
    text = remove_accents(text)
    heading_keyword = remove_accents(heading_keyword)
    if heading_block_keyword == 'project':
        if len(text) > len('project') +3:
            return False
    if (text.find(heading_keyword) < 5) and (len(text.split(' ')) - len(heading_keyword.split(' ')) < 6):
        return filter_sensitive(heading_keyword, case_sensitive)
    return False


def filter_strict_space_case(text, case_sensitive):
    # extract heading keyword from candiate rows
    heading_keyword = ''
    heading_block_keyword = ''
    for block_heading in ['working history', 'education', 'skills', 'information', 'objective', 'reference', 'awards', 'hobby', 'volunteer', 'project']:
        block_heading_variations = [remove_accents(f) for f in json_data_new[block_heading]]
        for block_heading_variation in block_heading_variations:
            if block_heading_variation == '':
                continue
            if block_heading_variation in text:
                heading_keyword = block_heading_variation
                heading_block_keyword = block_heading
                break
        if heading_keyword != '':
            break
    if heading_keyword == '':
        return False
    if heading_block_keyword == 'project':
        if len(text) > len('project') +3:
            return False
    # if (text.find(heading_keyword) < 5) and (len(text) - len(heading_keyword) < 20):
    if (text.find(heading_keyword) < 10):
        # return filter_sensitive(heading_keyword, case_sensitive)
        return True
    return False


def filter_heading_space_case(heading_df, list_heading=list_heading):
    """ Removed redundant rows non-including predefined heading keywords of given heading dataframe
    Input:
    heading_df: heading dataframe
    list_heading: list of pre-defined heading keywords
    Ouput:
    Heading dataframe, which removed redundant rows non-including predefined heading keywords
    """
    mask_heading = heading_df['clean_text'].apply(lambda x: np.any([remove_accents(f) in x for f in list_heading]))
    return heading_df[mask_heading]


def get_line_by_line_text(file):
    """ Get dataframe including text, boundingbox, fontsize, font, num_page of given pdf file
    Input:
    file: path of pdf file
    
    Ouput:
    Dataframe having columns:
    x0: left of bbox
    x1: right of bbox
    y0: top of bbox
    y1: bot of bbox
    text:
    size: size of text
    font: font of text
    page: which page that text lives in 
    label: cluster of bbox based of x0 (left of bbox)
    """
    doc = fitz.open(file)
    pages = doc.pageCount
    # fout = open(ofile,"w")
    list_texts = []
    for num_page in range(pages):
        pg = doc.loadPage(num_page)  
        # text = pg.getText(output = 'json')           # get its text in JSON format
        text = pg.getText('json')           # get its text in JSON format
        pgdict = json.loads(text)   

        for block in pgdict['blocks']:
            if block['type'] == 0:
                for line in block['lines']:
                    for span in line['spans']:
                        span['page'] = num_page
                        list_texts.append(span)

    line_df = pd.DataFrame(list_texts)
    line_df['x0'] = line_df['bbox'].apply(lambda x: x[0])
    line_df['x1'] = line_df['bbox'].apply(lambda x: x[2])
    line_df['y0'] = line_df['bbox'].apply(lambda x: x[1])
    line_df['y1'] = line_df['bbox'].apply(lambda x: x[3])
    line_df.drop(['bbox'], inplace=True, axis=1)
    for num_page in range(pages-1):
        mask_curr_page = line_df['page'] == num_page
        y1_end = line_df.loc[mask_curr_page, 'y1'].max()
        mask_next_page = line_df['page'] == (num_page +1)
        line_df.loc[mask_next_page, 'y0'] +=y1_end
        line_df.loc[mask_next_page, 'y1'] +=y1_end    

    clustering = DBSCAN(eps=15, min_samples=5)

    line_df['label'] = clustering.fit_predict(line_df['x0'].values.reshape(-1,1))
    line_df.reset_index(drop=True, inplace=True)
    return line_df


def get_block_text(file):
    """ Get dataframe including block text, boundingbox, num_page of given pdf file
    Input:
    file: path of pdf file
    
    Ouput:
    Dataframe having columns:
    x0: left of bbox
    x1: right of bbox
    y0: top of bbox
    y1: bot of bbox
    text:
    num_line: number of line of text
    page: which page that text lives in 
    """
    doc = fitz.Document(file)
    pages = doc.pageCount
    block_df = pd.DataFrame()
    for num_page in range(pages):
        pg = doc.loadPage(num_page)  
        blocks = pg.getTextBlocks()
        page_df = pd.DataFrame(blocks)
        page_df['page'] = num_page
        block_df = pd.concat((block_df, page_df), axis=0)

    block_df.drop(6, axis=1, inplace=True)
    block_df.columns = ['x0', 'y0', 'x1', 'y1', 'text', 'num_line', 'page']
    
    for num_page in range(0, pages-1):
        max_y1_curr = block_df.loc[block_df['page'] == num_page, 'y1'].max()

        mask_next_page = block_df['page'] == (num_page+1)
        block_df.loc[mask_next_page, 'y0'] += max_y1_curr
        block_df.loc[mask_next_page, 'y1'] += max_y1_curr   
    
    block_df.reset_index(drop=True, inplace=True)
    return block_df



s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ'
s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYy'
VIETNAMESE_PATTERN = {
    '[àáảãạăắằẵặẳâầấậẫẩ]': 'a',
    '[đ]': 'd',
    '[èéẻẽẹêềếểễệ]': 'e',
    '[ìíỉĩị]': 'i',
    '[òóỏõọôồốổỗộơờớởỡợ]': 'o',
    '[ùúủũụưừứửữự]': 'u',
    '[ỳýỷỹỵ]': 'y'
}





VIETNAMESE_PATTERN = {
    '[àáảãạăắằẵặẳâầấậẫẩ]': 'a',
    '[đ]': 'd',
    '[èéẻẽẹêềếểễệ]': 'e',
    '[ìíỉĩị]': 'i',
    '[òóỏõọôồốổỗộơờớởỡợ]': 'o',
    '[ùúủũụưừứửữự]': 'u',
    '[ỳýỷỹỵ]': 'y'
}



def remove_accents(data):

    for regex, replace in VIETNAMESE_PATTERN.items():
        data = re.sub(regex, replace, data)
        data = re.sub(regex.upper(), replace.upper(), data)
    data = re.sub(r'\s+', ' ', data)
    return data





def get_heading_key_word_from_text(text, list_heading_key_words):
    for key_word in list_heading_key_words:
        if key_word in text:
            return key_word
    return ''

def get_type_heading(heading_word):
    if heading_word.upper() == heading_word:
        return 3
    if heading_word.title() == heading_word:
        return 2
    return 1

def get_block_name(text, switch_filter):
    text = text.lower()
    for block_heading in ['working history', 'education', 'skills', 'information', 'objective',\
        'reference', 'awards', 'hobby', 'volunteer', 'project']:
        if switch_filter == 1:
            block_heading_variations = [remove_accents(f) for f in json_data[block_heading]]
        else:
            block_heading_variations = json_data[block_heading]
        for block_heading_variation in block_heading_variations:
            if block_heading_variation == '':
                continue
            if block_heading_variation in text:
                return block_heading
    return None


def get_heading_df(line_by_by_df, list_heading_key_words, space_case=0):
    """ Get heading dataframe including
    Input:
    line_by_by_df: dataframe generated from `get_line_by_line_text` function
    list_heading_key_words: List of heading keywords
    Ouput:
    Heading dataframe
    
    """
    # print(list_heading_key_words)
    switch_filter = 0
    heading_key_word = ''
    # for key_word in list_heading_key_words:
    line_by_by_df['key_word'] = line_by_by_df['text'].apply(get_heading_key_word_from_text, list_heading_key_words=list_heading_key_words)
    mask = line_by_by_df['key_word'].apply(lambda x: x != '')
    list_heading_key_words_clean = [remove_accents(f) for f in list_heading_key_words]
    if (mask.sum() > 0) and (space_case == 1):
        switch_filter = 1
        line_by_by_df['clean_text'] =  line_by_by_df['text'].apply(remove_accents)
        line_by_by_df['key_word'] = line_by_by_df['clean_text'].apply(get_heading_key_word_from_text, list_heading_key_words=list_heading_key_words_clean)
        mask = line_by_by_df['key_word'].apply(lambda x: x != '')
    if mask.sum() == 0:
        return None, None
    heading_rows = line_by_by_df[mask]
    heading_rows['case_sensitive_type'] = heading_rows['key_word'].apply(get_type_heading)
    heading_rows['len_heading'] = heading_rows['key_word'].apply(lambda x: len(x.split( )))
    ind = heading_rows.sort_values(by=['case_sensitive_type', 'size', 'len_heading'], ascending=[False, False, False]).index[0]
    # print(heading_rows.loc[ind])
    # print(heading_rows.sort_values(by=['case_sensitive_type', 'size', 'len_heading'], ascending=[False, False, False]))
    if switch_filter == 0:
        for key_word in list_heading_key_words:
            if key_word in heading_rows.loc[ind, 'text']:
                heading_key_word = key_word
                break
    else:
        for key_word in list_heading_key_words_clean:
            if key_word in heading_rows.loc[ind, 'clean_text']:
                heading_key_word = key_word
                break
    size_mark, text_mark, left_mark = line_by_by_df.loc[ind,['size', 'text', 'x0']]
    mask_heading_txt = line_by_by_df['size'] > float(size_mark)*0.8
    heading_df = line_by_by_df[mask_heading_txt]
    case_sensitive = 'upper'
    if heading_key_word == heading_key_word.upper():
        case_sensitive = 'upper'
    else: 
        if heading_key_word.split(' ')[0] == heading_key_word.split(' ')[0].capitalize():
            case_sensitive = 'capitalize'
    # mask_case_sensitive = heading_df['text'].apply(filter_sensitive, cond=case_sensitive)
    # heading_df = heading_df[mask_case_sensitive]
    # heading_df = heading_df[heading_df['text'].apply(lambda x: len(x)) > 2]
    if switch_filter == 0:
        mask_strict_filter = heading_df['text'].apply(filter_strict, case_sensitive=case_sensitive)
        heading_df = heading_df[mask_strict_filter]
    else:
        mask_strict_filter = heading_df['clean_text'].apply(filter_strict_space_case, case_sensitive=case_sensitive)
        heading_df = heading_df[mask_strict_filter]
    clustering = DBSCAN(eps=150, min_samples=2)
    if heading_df.shape[0] == 0:
        return None, None
    heading_df['label'] = clustering.fit_predict(heading_df['x0'].values.reshape(-1,1))
    # print(switch_filter)
    heading_df['block_name'] = heading_df['text'].apply(get_block_name, switch_filter=switch_filter)
    return heading_df, heading_key_word



def get_constraint_block(heading_df, heading_keyword, space_case=0):
    """ Get a bunch of contraints of given heading keyword (top, bot, x_boundary, label_block)
    Input: 
    heading_df: DataFrame of heading
    heading_keyword: heading keyword
    
    Ouput:
    top: top of block
    bot: bot of block
    x_boundary: consider when 2-columns CVs, return -1 when Cvs be not type of 2 columns
    label_block: label of cluster of given heading keywords
    """
    clustering = DBSCAN(eps=100, min_samples=2)
    heading_df['label'] = clustering.fit_predict(heading_df['x0'].values.reshape(-1,1))
    # re-order label cluster by x0 (horizontal coordinate)
    ranking_heading = heading_df.groupby('label')['x0'].min().reset_index()
    ranking_heading['rank'] = np.argsort(ranking_heading['x0'])
    heading_df['label'] = heading_df['label'].map(dict(zip(ranking_heading['label'], ranking_heading['rank'])))
    # print(heading_df)

    # case 2 columns, find line boundary
    x_boundary = -1
    if (heading_df['label'].nunique() == 2) & (sum(heading_df['label'] == 1) != 1):
        x_boundary = 0.5 * (min(heading_df.groupby('label')['x1'].max()) + max(heading_df.groupby('label')['x0'].min()))-10
    if space_case == 0:
        mask_block = heading_df['text'].apply(lambda x: heading_keyword in x)
    else:
        mask_block = heading_df['clean_text'].apply(lambda x: heading_keyword in x)
    label_block = heading_df.loc[mask_block, 'label'].values[0]
    heading_df = heading_df[heading_df['label'] == label_block]
    heading_df = heading_df.sort_values(by='y0').reset_index(drop=True)
    # print(heading_df)
    # after removing text out of heading block
    if space_case == 0:
        mask_block_new = heading_df['text'].apply(lambda x: heading_keyword in x)
    else:
        mask_block_new = heading_df['clean_text'].apply(lambda x: heading_keyword in x)
    index_block = heading_df[mask_block_new].index[0]
    # print(heading_df.loc[index_block])
    # find top and bot of given block
    next_index_block = index_block + 1
    bot = 0
    top = 0
    while True:
        # case given block in the end of document
        if next_index_block == heading_df.shape[0]:
            top = heading_df.loc[index_block,['y0']].values[0] # case end of document
            break
        # case heading and normalize having same style (need to fix more)
        if (heading_df.loc[next_index_block, 'y0'] - heading_df.loc[index_block, 'y0']) <10: # should tune more 
            next_index_block+=1
        else:
            break
    # normal case, containing top and bot constraints
    if top == 0: 
        top, bot = heading_df.loc[[index_block,next_index_block],'y0'].values
    # print(top, bot)
    return top, bot, x_boundary, label_block


def get_font_block_df(row, line_by_line_df):
    x0, x1, y0, y1, page = row[['x0', 'x1', 'y0', 'y1', 'page']]
    mask = (line_by_line_df['x0'] >= x0) & (line_by_line_df['x1'] <= x1) & (line_by_line_df['y0'] >= y0) & (line_by_line_df['y1'] <=y1) & (line_by_line_df['page'] == page)
    return line_by_line_df.loc[mask, 'size'].max()


def get_text_correspoinding_keyword(heading_df,heading_key_word, block_df, space_case):
    """Get text of heading in heading_df
    Input:
    heading_df
    heading_key_word: get from heading_df
    block_df
    space_case: capture heading word variation
    """
    top, bot, x_boundary, label_block = get_constraint_block(heading_df, heading_key_word, space_case=space_case)

    try:
        if bot != 0:
            mask_top_bot_block = (block_df['y0'] >= top) & (block_df['y1'] <= bot)
        else:
            mask_top_bot_block = (block_df['y0'] >= top)  
        if x_boundary != -1:
            if label_block == 0:
                mask_top_bot_block = mask_top_bot_block & (block_df['x0'] < x_boundary)
            elif label_block == 1:
                mask_top_bot_block = mask_top_bot_block & (block_df['x0'] > x_boundary)
    except:
        return []
    # print(mask_top_bot_block)
    # return line_by_line_df.loc[mask_top_bot, 'text'].values.tolist()
    # return block_df.loc[mask_top_bot_block, 'text'].values
    return mask_top_bot_block


def get_heading_key_word(block_name):
    with open('SAP_info/data/list_heading.json') as json_file:
        json_data = json.loads(json_file.read())
    heading_key_words = json_data[block_name] + [block_name]
    heading_key_words = [f for f in heading_key_words if f != '']
    heading_key_words.sort(key = lambda s: len(s.split( )), reverse=True)
    heading_new = []
    for heading_word in heading_key_words:
        temp_word = heading_word.upper()
        heading_new.append(temp_word)
    for heading_word in heading_key_words:
        temp_word = heading_word.title()
        heading_new.append(temp_word)
    for heading_word in heading_key_words:
        temp_word = heading_word.capitalize()
        heading_new.append(temp_word)

    return heading_new


def get_block_text_corresponding_heading(file, list_heading_key_words):
    """ Get block text of given list_heading_key_words
    Input:
    file: path of pdf file
    list_heading_key_words: list of heading keywords
    
    Output:
    List of text of given heading keywords
    """
    space_case = 0
    line_by_line_df = get_line_by_line_text(file)
    block_df = get_block_text(file)
    list_heading_key_words = get_heading_key_word('working history')
    heading_df, heading_keyword = get_heading_df(line_by_line_df, list_heading_key_words)

    if heading_keyword is None:
        space_case = 1
        block_df['size'] = block_df.apply(get_font_block_df, line_by_line_df=line_by_line_df, axis=1)
        heading_df, heading_keyword = get_heading_df(block_df, list_heading_key_words, space_case=space_case)
    if space_case == 0:
        keywords = heading_df['text'].values
    else:
        keywords = heading_df['clean_text'].values
    # print(f'{Fore.RED} Output {Style.RESET_ALL}')
    # print(heading_df)
    mask_total = [False]*block_df.shape[0]
    for keyword in keywords:
        # print(f'{Fore.GREEN} {keyword} {Style.RESET_ALL}')
        mask_single_block = get_text_correspoinding_keyword(heading_df, keyword, block_df, space_case=space_case)
        mask_total += mask_single_block
        pprint.pprint(block_df.loc[mask_single_block, 'text'].values)



def main():
    file_name = '/Users/quan/Desktop/939697e7-971e-4b66-8337-85e812479eb5-1.pdf'
    block_df = get_block_text(file_name)


if __name__ == '__main__':
    main()
