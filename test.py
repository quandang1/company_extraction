import pandas as pd
import numpy as np
import os
from utils import get_line_by_line_text, remove_accents, company_keyword_processor, get_special_line,\
                  get_case_sensitive_keyword
import subprocess
import warnings
warnings.filterwarnings('ignore')



def main():
    # original_path = '/Users/quan/Desktop/jobhop/education/new_location/test_data'
    # original_path = '/Users/quan/Desktop/jobhop_2/2020/10/ai_parser/sample_data'
    original_path = '/Users/quan/Desktop/full_parsing_strategy/cv_parser/List of CV'
    all_files = os.listdir(original_path)
    all_files = [os.path.join(original_path, f) for f in all_files if f.endswith('.pdf')]
    individual_file = np.random.choice(all_files)
    individual_file = os.path.join(original_path, 'buihungmanh_giam doc du an xay dung.pdf')
    # individual_file = '/Users/quan/Desktop/jobhop_2/2020/10/ai_parser/sample_data/7770752.pdf'
    # individual_file = '/Users/quan/Desktop/jobhop_2/2020/10/ai_parser/sample_data/3801098.pdf'
    print(individual_file)
    subprocess.call(['open', individual_file]) 
    df = get_line_by_line_text(individual_file)
    df['cleaned_text'] = df['text'].apply(remove_accents)
    df['company_keyword'] = df['cleaned_text'].apply(lambda x: company_keyword_processor.extract_keywords(x))
    special_line_df = get_special_line(df)
    mask_company = df['company_keyword'].apply(lambda x: len(x) !=0)
    company_keyword_df = df[mask_company]
    mask_case_sensitive = company_keyword_df.apply(get_case_sensitive_keyword, axis=1)
    print(company_keyword_df.shape)
    company_keyword_df = company_keyword_df[mask_case_sensitive]
    print(company_keyword_df.shape)
    print(company_keyword_df[['text', 'company_keyword']])
    print(company_keyword_df['text'].values.tolist())
    return 
    special_df = pd.concat((special_line_df, df[mask_company]), axis=0)
    print(special_df.shape)
    print(special_line_df.shape)
    print(df[mask_company].shape)
    print(special_df['text'].values.tolist())
if __name__ == "__main__":
    main()