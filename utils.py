import pandas as pd
import json
from sklearn.cluster import DBSCAN
import fitz
from constants import s0, s1, COMPANY_KEYWORDS
import re
from flashtext import KeywordProcessor
import numpy as np


def get_line_by_line_text(file):
    """ Get dataframe including text, boundingbox, fontsize, font, num_page of given pdf file
    Input:
    file: path of pdf file
    
    Ouput:
    Dataframe having columns:
    x0: left of bbox
    x1: right of bbox
    y0: top of bbox
    y1: bot of bbox
    text:
    size: size of text
    font: font of text
    page: which page that text lives in 
    label: cluster of bbox based of x0 (left of bbox)
    """
    doc = fitz.open(file)
    pages = doc.pageCount
    list_texts = []
    for num_page in range(pages):
        pg = doc.loadPage(num_page)  
        # text = pg.getText(output = 'json')           # get its text in JSON format
        text = pg.getText('json')           # get its text in JSON format
        pgdict = json.loads(text)   

        for block in pgdict['blocks']:
            if block['type'] == 0:
                for line in block['lines']:
                    for span in line['spans']:
                        span['page'] = num_page
                        list_texts.append(span)

    line_df = pd.DataFrame(list_texts)
    line_df['x0'] = line_df['bbox'].apply(lambda x: x[0])
    line_df['x1'] = line_df['bbox'].apply(lambda x: x[2])
    line_df['y0'] = line_df['bbox'].apply(lambda x: x[1])
    line_df['y1'] = line_df['bbox'].apply(lambda x: x[3])
    line_df.drop(['bbox'], inplace=True, axis=1)
    for num_page in range(pages-1):
        mask_curr_page = line_df['page'] == num_page
        y1_end = line_df.loc[mask_curr_page, 'y1'].max()
        mask_next_page = line_df['page'] == (num_page +1)
        line_df.loc[mask_next_page, 'y0'] +=y1_end
        line_df.loc[mask_next_page, 'y1'] +=y1_end    

    clustering = DBSCAN(eps=15, min_samples=5)

    line_df['label'] = clustering.fit_predict(line_df['x0'].values.reshape(-1,1))
    line_df.reset_index(drop=True, inplace=True)
    return line_df

def remove_accents(input_str):
    input_str = input_str.lower()
    s = ""
    for c in str(input_str):
        if c in s1:
            s += s0[s1.index(c)]
        else:
            s += c
    return s


company_keyword_processor = KeywordProcessor()
company_keyword_processor.add_keywords_from_list(COMPANY_KEYWORDS)



def get_special_lines_corresponding_feature(line_df, feature):
    """
    feature can be `size`, `flags`, `color`
    """
    partition_df = line_df[feature].value_counts(normalize=True).reset_index()
    valid_special_values = partition_df['index'].values[1:]
    mask_feature = line_df[feature].isin(valid_special_values)
    return np.where(mask_feature.values)[0]

def case_sensitive_line(line):
    if re.search(r'20\d{2}', line):
        return True
    words = line.split()
    count_sensitive = 0
    count_missing = 0
    for word in words:
        if count_missing > 5 and count_sensitive == 1:
            count_sensitive = 0
        if count_sensitive >=2:
            return True
        if word != word.lower():
            count_sensitive +=1
            count_missing = 0
        else:
            count_missing +=1
    if len(words) == 1 and count_sensitive == 1:
        return True
    return False

def get_special_line(line_df):
    indices_flags = get_special_lines_corresponding_feature(line_df, 'flags')
    indices_size = get_special_lines_corresponding_feature(line_df, 'size')
    indices_color = get_special_lines_corresponding_feature(line_df, 'color')
    indices_special = np.unique(np.concatenate([indices_flags, indices_color, indices_size]))
    special_df = line_df.iloc[indices_special]
    special_df.loc[:,'filtered'] = special_df['text'].apply(case_sensitive_line)
    special_df = special_df[special_df['filtered']]
    return special_df

def get_case_sensitive_keyword(row):
    text = row['text']
    cleaned_text = row['cleaned_text']
    keywords = row['company_keyword']
    for keyword in keywords:
        begin = cleaned_text.find(keyword)
        end = begin + len(keyword)
        original_keyword = text[begin:end]
        if original_keyword != original_keyword.lower():
            return True

    return False

